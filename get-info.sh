#!/bin/sh

path=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
yesterday=`date -d "yesterday 13:00" '+%Y-%m-%d'`
date=`date "+%Y-%m-%d"`
time=`date "+%H-%M-%S"`

declare -A s

s[ip]="`curl -s -4 https://ifconfig.co/`"
#s[os]="`cat /etc/os-release`"
s[os]="Arch Linux"
#s[hardware]="`uname -m`"
s[hardware]="x86_64"
#s[host]="`cat /sys/devices/virtual/dmi/id/product_version`"
s[host]="M720q"
s[kernel]="`uname -r`"
#s[uptime]="`cat /proc/uptime`"
s[uptime]="`uptime -p`"
s[uptime]=${s[uptime]:3}
s[datetime]="`date +%Y/%m/%d\ %H:%M:%S\ %Z`"
s[location]="Strasbourg (FR)"
s[temp]="$((`cat /sys/devices/virtual/thermal/thermal_zone0/temp` / 1000))°C"
s[storage_1_path]="/dev/nvme0n1p3"
s[storage_1_total]="`df -h | grep ${s[storage_1_path]} | awk '{print $2'}`"
s[storage_1_used]="`df -h | grep ${s[storage_1_path]} | awk '{print $3'}`"
s[storage_1_avail]="`df -h | grep ${s[storage_1_path]} | awk '{print $4'}`"
s[storage_1_percent]="`df -h | grep ${s[storage_1_path]} | awk '{print $5'}`"
s[storage_1]="${s[storage_1_used]}/${s[storage_1_total]}" #(${s[storage_1_percent]})"
s[storage_2_path]="/dev/sdb1"
s[storage_2_total]="`df -h | grep ${s[storage_2_path]} | awk '{print $2'}`"
s[storage_2_used]="`df -h | grep ${s[storage_2_path]} | awk '{print $3'}`"
s[storage_2_avail]="`df -h | grep ${s[storage_2_path]} | awk '{print $4'}`"
s[storage_2_percent]="`df -h | grep ${s[storage_2_path]} | awk '{print $5'}`"
s[storage_2]="${s[storage_2_used]}/${s[storage_2_total]}" #(${s[storage_2_percent]})"
s[memory_total]="`free -h | grep Mem | awk '{print $2}'`"  #total amount of physical RAM
s[memory_used]="`free -h | grep Mem | awk '{print $3}'`"   #used RAM
s[memory_avail]="`free -h | grep Mem | awk '{print $7}'`"  #estimated available memory
s[memory]="${s[memory_used]}/${s[memory_total]}" #(${s[memory_avail]})"
s[loadavg_1]="`cat /proc/loadavg | awk '{print $1}'`"   #1m interval
s[loadavg_5]="`cat /proc/loadavg | awk '{print $2}'`"   #5m interval
s[loadavg_15]="`cat /proc/loadavg | awk '{print $3}'`"  #15m interval
s[speed_upload]="`cat $path/speedtest.json | jq -r '.[0].upload'` Mbps"
s[speed_download]="`cat $path/speedtest.json | jq -r '.[0].download'` Mbps"
s[power_now]="`curl http://192.168.1.110/rpc/Switch.GetStatus?id=0 | jq -r '.apower'` W"
s[power_yesterday_kWh]="`cat $path/power/yesterday | awk '{print $2}'`"
s[power_yesterday_cost]="`cat $path/power/yesterday | awk '{print $3}'`"
s[power_yesterday]=$(printf "%.3f kWh (%.3f €)\n" ${s[power_yesterday_kWh]} ${s[power_yesterday_cost]}) 

# more stats??
# - uptime percent since date / uptime history (uptimed)?
# - ip changes/history (gandi-dns-update)

#s[cpu_usage]="`echo $[100-$(vmstat 1 2|tail -1|awk '{print $15}')]` %" #idk about this
# CPU load is defined as the number of processes using or waiting to use one core at a single point in time
# CPU usage is the percentage of time a CPU takes to process non-idle tasks
#
#
#              total        used        free      shared  buff/cache   available
#mem:          2.8gi       1.1gi       283mi       224mi       1.4gi       1.2gi
#swap:         3.0gi       881mi       2.1gi
#it is important to note the difference between "free" and "available" memory. in the above example, a laptop with 2.8 gib of total ram appears to be using most of it, with only 283 mib as free memory. however, 1.4 gib of it is "buff/cache". there is still 1.2 gib available for starting new applications, without swapping.o
#2.8-1.1=1.7


# save power info in today file
echo ${s[power_now]::-2} >> $path/power/today


json="{"
for i in "${!s[@]}"
do
    json+="\n"
    json+=\"$i\":\"${s[$i]}\",
done
json=${json::-1}
json+="\n}"

echo -e $json
