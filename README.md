# homeserver

my homeserver website and config

## Requirements 

- librespeed-cli
- jq
- bc

## Install

edit crontab -e

    */1 * * * * sh /home/emile/public_html/homeserver/get-info.sh > /home/emile/public_html/homeserver/info.json
    */5 * * * * sleep 20; librespeed-cli -4 --server 50 --duration 10 --json > /home/emile/public_html/homeserver/speedtest.json
    59 23 * * * sleep 30; sh /home/emile/public_html/homeserver/power-average.sh
