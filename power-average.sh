#!/bin/sh

path=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
#yesterday=`date -d "yesterday 13:00" '+%Y-%m-%d'`
date=`date "+%Y-%m-%d"`

len=0
sum=0

while read line; do 
    sum=$(echo $sum + $line | bc -l);
    len=$((len+1))
done < $path/power/today




#kWh cost (2023/11/25)
#HC=0.1513
#HP=0.2040
#(HC*8+HP*16)/24=0.18643333333333

average=$(echo $sum / $len | bc -l)
Wh=$(echo ${average}*24 | bc -l)
kWh=$(echo ${average}*24/1000 | bc -l)
cost=$(echo ${average}*24/1000*0.18643 | bc -l)

echo "len = ${len}"
echo "sum = ${sum}"
echo "average = ${average}"
echo "kWh = ${kWh}"
echo "cost = ${cost}"



# keep archive of the daily data
printf "%s %.4f %.4f %.4f\n" ${date} ${average} ${kWh} ${cost} >> $path/power/days

# save yesterday's data in specific file
printf "%.4f %.4f %.4f\n" ${average} ${kWh} ${cost} > $path/power/yesterday

# clean today's data
> $path/power/today
